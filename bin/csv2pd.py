#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import csv
import pandas as pd
import pickle
import argparse

def parse_args():
    parser = argparse.ArgumentParser(prog="csv2pd",
        description="convert csv to pandas DataFrame and save as pickle")

    # parser.add_argument('--instrument', help='instrument symbol')
    # parser.add_argument('--end-date', help='end date')
    # parser.add_argument('--start-date', help='start date')
    parser.add_argument('input', help='input file')
    parser.add_argument('--output', help='output file')
    parser.add_argument('--output-format', default='pkl', help='output format')
    parser.add_argument('--group', help='group field')
    parser.add_argument('--time', help='time field')
    parser.add_argument('--nrows', help='number of rows to read')
    # parser.add_argument('--db-engine', default='mongodb', help='which database to use')

    args, unknown = parser.parse_known_args()
    print('command line argument parsed:', args)
    print('unknown args:', unknown)
    return args, unknown

def csv2pd(infile, outfile, args, unknown):
    if args.output_format == 'pkl' and not outfile.endswith('.pkl'):
        outfile += '.pkl'

    print(f'converting file {infile} to {outfile}')

    kwds, group, set_idx = _infer_opts(infile)
    if args.group is not None:
        group = args.group.split(',')
    if args.time is not None:
        arr = args.time.split(',')
        kwds['parse_dates'] = arr
        set_idx = arr

    # print(kwds, group, set_idx)
    df = pd.read_csv(infile, quoting=csv.QUOTE_NONNUMERIC, nrows=args.nrows, **kwds)
    # print(df)
    # print(df.describe())
    # print(df.columns)
    # print(df.dtypes)
    # print(df.index)
    # print(dir(df.index))

    if set_idx is not None:
        _if_set_idx = lambda d: d.set_index(set_idx)
    else:
        _if_set_idx = lambda d: d

    out = {}
    if group is not None:
        for k, g in df.groupby(group):
            print(f'processing group {k}, number of records: {len(g.index)}')
            out[k] = _if_set_idx(g)
    else:
        out = _if_set_idx(df)

    # print(out)
    print(f'dumping to file {outfile}')
    with open(outfile, 'wb') as f:
        pickle.dump(out, f)

    print('done.')

def _infer_opts(infile):
    with open(infile) as f:
        header = next(csv.reader(f))
        print(f'header of csv: {header}')

    idx = { k:i for i, k in enumerate(header)}
    kwds = {}
    group = None
    set_idx = None

    if 'symbol' in idx:
        kwds.setdefault('dtype', {})['symbol'] = str
        # kwds.setdefault('index_col', []).append(idx['symbol'])
        group = ['symbol']
        print('has symbol column, keep it string and use it for grouping')

    date_field = '日期'
    idate = idx.get(date_field, None)
    if idate is not None:
        kwds['parse_dates'] = [idate]
        # kwds.setdefault('index_col', []).append(idate)
        set_idx = [date_field]
        print(f'has date column {date_field}')

    return kwds, group, set_idx

def main():
    args, unknown = parse_args()
    infile = args.input

    if args.output is None:
        d = os.path.dirname(infile)
        fname = os.path.basename(infile)
        stem, ext = os.path.splitext(fname)
        outfile = os.path.join(d, stem)
    else:
        outfile = args.output

    csv2pd(infile, outfile, args, unknown)
    return

if __name__ == '__main__':
    main()
