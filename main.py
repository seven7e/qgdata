#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__)))

import qgdata.app as app

def main():
    app.main(sys.argv[1:])

if __name__ == '__main__':
    main()
