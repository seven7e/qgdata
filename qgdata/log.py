#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import logging

__all__ = ['log']

level = os.environ.get('LOGLEVEL', 'INFO').upper()

# create logger
logger = logging.getLogger('qgdata')
logger.setLevel(level)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(level)

# create formatter
formatter = logging.Formatter('%(asctime)s [%(filename)s:%(funcName)s:%(levelname)s] %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

# 'application' code
# logger.debug('debug message')
# logger.info('info message')
# logger.warning('warn message')
# logger.error('error message')
# logger.critical('critical message')

log = logger

def main():
    return

if __name__ == '__main__':
    main()
