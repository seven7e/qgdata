#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
from typing import Tuple
from dotenv import load_dotenv

INDEX_PH = '__index__'

ENV = os.getenv('ENV', 'dev')

def must_int(name, err_msg=None) -> int:
    s = must_value(name, err_msg)
    try:
        i = int(s)
        return i
    except:
        msg = err_msg if err_msg is not None else f'{name} must be an int'
        raise RuntimeError(msg)

def must_str(name, err_msg=None) -> str:
    s = must_value(name, err_msg)
    if s == '':
        msg = err_msg if err_msg is not None else f'{name} must be a non-empty string'
        raise RuntimeError(msg)
    return s

def must_value(name, err_msg=None) -> str:
    v = os.getenv(name)
    if v is None:
        msg = err_msg if err_msg is not None else f'{name} is not set'
        raise RuntimeError(msg)
    return v

def get(name, default=None):
    v = os.getenv(name)
    if v is None:
        return default
    return v

def mongodb_addr() -> Tuple[str, int]:
    host = must_str('MONGODB_HOST')
    port = must_int('MONGODB_PORT')
    return host, port

def mongodb_auth() -> Tuple[str, str, str]:
    user = must_str('MONGODB_USER')
    password = must_str('MONGODB_PASSWORD')
    auth_db = must_str('MONGODB_AUTH_DB')
    return user, password, auth_db

def mongodb_data_db(default=None) -> str:
    return get('MONGODB_DATA_DB', default)

def load(dir: str = '.') -> None:
    if ENV == 'prod':
        load_dotenv('.env.prod')
    elif ENV == 'staging':
        load_dotenv('.env.staging')
    elif ENV == 'dev':
        load_dotenv('.env.dev')
    load_dotenv('.env')

def main():
    return

if __name__ == '__main__':
    main()
