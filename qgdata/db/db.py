#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import pandas as pd

from ..log import log
from .. import conf

_ID_SEP = '\x1F'

class Table(object):

    def insert_df(self, df, id_fields=None, soft_delete=False):
        if not isinstance(df, pd.DataFrame):
            raise RuntimeError('param must be a pandas dataframe')

        column = list(df.columns)
        if 'index' in column or conf.INDEX_PH in column:
            raise RuntimeError(f'do not support any column named "index" or "{conf.INDEX_PH}"')

        if conf.INDEX_PH in id_fields:
            column.insert(0, conf.INDEX_PH)
            data = df.reset_index().values.tolist()
        else:
            data = df.values.tolist()

        has_id = id_fields is not None and len(id_fields) > 0
        log.info('data contains following fields: %s', str(column))
        log.info('fields as ID: %s', 'auto generated' if not has_id else str(id_fields))
        # print(data)

        if has_id:
            column.append('__id__')
            id_idx = []
            for f in id_fields:
                i = column.index(f)
                if i < 0:
                    raise RuntimeError('can not find id fields ' + str(f))
                id_idx.append(i)

            # print(id_idx)
            for d in data:
                # print(d)
                # print(d[0])
                # print(d[0].timestamp())
                # print(type(d[0]))
                # sys.exit()
                ids = [ d[i] for i in id_idx ]
                # print(ids)
                id_ = _ID_SEP.join(_format_id(i) for i in ids)
                d.append(id_)

            _check_duplicates(data)

        self.insert(column, data, id_field='__id__', upsert=True, soft_delete=soft_delete)

def _format_id(i):
    if isinstance(i, pd.Timestamp):
        return str(int(i.timestamp() * 1000))
    # print(i)
    # if isinstance(i, pd.)
    return str(i)

def _check_duplicates(data, id_index=-1):
    grouped = {}
    for d in data:
        id_ = d[id_index]
        grouped.setdefault(id_, []).append(data)

    for k, v in grouped.items():
        if len(v) > 1:
            print(f'duplication detected: key={k}')
            for i, d in enumerate(v):
                print(f'\t{d}')


def main():
    return

if __name__ == '__main__':
    main()
