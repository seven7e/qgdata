#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from pymongo import MongoClient  # type: ignore
from pymongo import InsertOne, DeleteMany, ReplaceOne, UpdateOne # type: ignore
from pymongo import ASCENDING, DESCENDING
from .. import conf
from ..log import log
from .. import utils
from .db import Table

class _MongoTable(Table):

    def __init__(self, table):
        super().__init__()
        self.__table = table

    def __str__(self):
        return f'{self.__class__.__name__}({str(self.__table)})'

    def name(self):
        return self.__table.name

    def insert(self, column, data, id_field=None, upsert=True, soft_delete=False, bulk_size=1000):
        num_data = len(data)

        if num_data == 0:
            log.info('data size zero, skip it')
            return

        for d in data:
            if len(column) != len(d):
                raise RuntimeError(f'column and data length doesn\'t match: {column} <-> {d}')

        def _k(k):
            return k if k != id_field else '_id'

        def _insert_bulk(bulk_data):
            basons = [ { _k(k):v for k, v in zip(column, d)} for d in bulk_data ]
            # print(basons)
            log.info('%d records to save...', len(basons))
            if not upsert:
                log.info('insert record without upsert')
                self.__table.insert_many(basons)
            elif '_id' in basons[0]:
                bulk_ops = []
                ids = [ b['_id'] for b in basons ]
                result = self.__table.find({'_id': {'$in': ids}})
                objs_dict = { d['_id']: d for d in result }
                # print(objs_dict)
                num_rec_to_change = 0
                for b in basons:
                    query = {'_id': b['_id']}
                    obj_in_db = objs_dict.get(b['_id'], None)
                    if obj_in_db is None:
                        # self.__table.insert_one(b)
                        bulk_ops.append(InsertOne(b))
                        num_rec_to_change += 1
                    elif b == obj_in_db:
                        # print('identical, skip')
                        continue
                    elif soft_delete:
                        # self.__table
                        raise NotImplementedError('soft deletion is not supported')
                    else:
                        log.debug('not identical, replacing id=%s, diff=`%s`', \
                            b['_id'], str(utils.dict_diff(obj_in_db, b)))
                        # self.__table.replace_one(query, b)
                        bulk_ops.append(ReplaceOne(query, b))
                        num_rec_to_change += 1
                    # print(obj_in_db)
                # print(bulk_ops)
                # sys.exit()
                log.info('number of records to change: %d', num_rec_to_change)
                log.info('size of bulk operations: %d', len(bulk_ops))
                if len(bulk_ops) > 0:
                    ret = self.__table.bulk_write(bulk_ops)
                    log.info('mongodb bulk operation result: %s, acknowledged: %s', ret.bulk_api_result, ret.acknowledged)
            else:
                log.info('insert record without auto-generated id')
                self.__table.insert_many(basons)

        start = 0
        # bulk_size = 10
        while start < num_data:
            end = start + bulk_size
            bulk = data[start : end]
            log.info('inserting bulk (%d..%d)/%d', start, start+len(bulk), num_data)
            _insert_bulk(bulk)
            start += bulk_size

    def insert_jsons(self, jsons):
        return self.__table.insert_many(jsons)

    def find_max(self, field, ret_fields=None, series=None):
        return self._sort_one(field, DESCENDING, ret_fields, series)

    def find_min(self, field, ret_fields=None, series=None):
        return self._sort_one(field, ASCENDING, ret_fields, series)

    def groupby(self, group_fields, aggr_fields):
        g = {
            '$group': dict(
                _id={ k: '$'+k for k in group_fields },
                **{f: {'$'+a: '$'+f} for f, a in aggr_fields.items()}
            )
        }

        p = [ g ]
        # print(p)
        ret = self.__table.aggregate(p)
        # print(list(ret))
        return { tuple(r['_id'][f] for f in group_fields): {f:r[f] for f in aggr_fields} for r in ret }

    def _sort_one(self, field, direction, ret_fields, series):
        if series is None:
            query = {}
        elif isinstance(series, dict):
            query = series
        else:
            raise RuntimeError('unsupported series format: ' + repr(series))
        # print(query)

        if ret_fields is None:
            proj = None
            _post = lambda d: d
        elif isinstance(ret_fields, str) :
            proj = { ret_fields: 1, '_id': 0 }
            _post = lambda d: d[ret_fields]
        elif isinstance(ret_fields, (tuple, list)):
            proj = { k: 1 for k in ret_fields}
            proj['_id'] = 0
            _post = lambda d: tuple(d[f] for f in ret_fields)
        else:
            raise RuntimeError('unsupported fields: ' + repr(ret_fields))

        if isinstance(field, str):
            res = self.__table.find(query, proj).sort(field, direction).limit(1)
        else:
            s = [ (k, direction) for k in field ]
            # print(s)
            res = self.__table.find(query, proj).sort(s).limit(1)

        for d in res:
            return _post(d)

    def count(self):
        return self.__table.count()

    def find(self, *args, **kwds):
        return self.__table.find(*args, **kwds)

    def find_all(self):
        return self.__table.find()

    def delete_all(self):
        log.warn('deleting all records in table %s', self.name())
        return self.__table.delete_many({})

    def delete_by_id(self, id):
        return self.__table.delete_one({'_id': id})

class MongoDao(object):

    def __init__(self, client, db_name):
        self.__client = client
        self.__db = self.__client[db_name]

    def __str__(self):
        return f'{self.__class__.__name__}({str(self.__client)})'

    def get_tables(self):
        return self.__db.list_collection_names()

    def get_table(self, table_name, create_if_not_exist=False):
        if not create_if_not_exist:
            check = self.__db.list_collection_names(filter={'name': table_name})
            # print(check)
            if len(check) == 0:
                raise RuntimeError(f'table {table_name} does not exist')
        # log.debug('get table %s', table_name)
        return _MongoTable(self.__db[table_name])

_client = None
def get_client():
    global _client
    if _client is None:
        host, port = conf.mongodb_addr()
        user, password, auth_db = conf.mongodb_auth()

        _client = MongoClient(host,
            port=port,
            username=user,
            password=password,
            authSource=auth_db,
            authMechanism='SCRAM-SHA-256')

        log.info('connect to mongodb %s@%s:%d', user, host, port)
    return _client

def create(db_name=None):
    if db_name is None:
        _, _, auth_db_name = conf.mongodb_auth()
        db_name = conf.mongodb_data_db(auth_db_name)
    db = MongoDao(get_client(), db_name)
    log.info('connect to database %s', db_name)
    return db

def _test_insert():
    c = create('test')
    print(c)
    t = c.get_table('foobar')
    t.delete()
    print('before')
    print(list(t.find()))
    t.insert(['x', 'y', 'zz'], [[100, 200, 300], [-1, -22, -333]], id_field='x')
    print('after')
    print(list(t.find()))
    print('duplicated')
    t.insert(['x', 'y', 'zz'], [[100, 200, 300], [-1, -22, -333]], id_field='x')

def _test_insert_df():
    import pandas as pd

    c = create('test')
    print(c)
    t = c.get_table('foobar')
    t.delete()
    print('before')
    print(list(t.find()))
    df = pd.DataFrame(dict(x=[1, 2, 1], y=[100,101, 200]))
    t.insert_df(df, id_fields=['x', 'y'])
    print('after')
    print(list(t.find()))
    print('duplicated')
    df = pd.DataFrame(dict(x=[1,2, 1], y=[100,101, 200]))


def main():
    # _test_insert()
    _test_insert_df()
    return

if __name__ == '__main__':
    main()
