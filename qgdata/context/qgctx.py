#!/usr/bin/env python
# -*- coding: utf-8 -*-

from . import context

_API_KEY = context.new_key()
_DB_KEY = context.new_key()

def ctx_with(ctx, **kwds):
    return context.MapContext(ctx, **kwds)

def lazy_ctx_with(ctx, api=None, db=None):
    kwds = {}

    def _add_if(key, v):
        if v is not None:
            kwds[key] = v

    _add_if(_API_KEY, api)
    _add_if(_DB_KEY, db)

    return context.LazyContext(ctx, **kwds)

def api(ctx):
    return ctx.get(_API_KEY)

def db(ctx):
    return ctx.get(_DB_KEY)