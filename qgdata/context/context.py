#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import sys

_id = 0

def new_key() -> int:
    global _id
    _id += 1
    return '__' + str(_id)

class _NotFound(object): pass
_NOT_FOUND = _NotFound()

class Context(object):
    def __init__(self, parent):
        self.__parent = parent

    def _not_found(self):
        return _NOT_FOUND

    def must_get(self, key):
        v = self.get(key)
        if v is _NOT_FOUND:
            raise RuntimeError(f'key not found in context: {key}')
        return v

    def get(self, key):
        # print(f'get key: {key}...')
        v = self._get(key)
        # print(f'got key: {key} -> {v}')
        if v is not _NOT_FOUND:
            return v
        if self.__parent is not None:
            return self.__parent.get(key)
        return self._not_found()

class MapContext(Context):
    def __init__(self, parent=None, **kwds):
        super().__init__(parent)
        self._entries = kwds

    # def __put(self, key, value):
    #     print(f'put {key} <- {repr(value)}')
    #     self.__entries[key] = value
    #     return self

    def _get(self, key):
        # print(f'_get key {key}')
        return self._entries.get(key, self._not_found())

class _Uninitialized(object): pass
_UNINITIALIZED = _Uninitialized()

class LazyContext(MapContext):
    def __init__(self, parent=None, **kwds):
        super().__init__(parent, **({k: _UNINITIALIZED for k in kwds}))
        # self.__initializers = {}
        self.__initializers = kwds

    # def put(self, key, initializer):
    #     print(f'put lazy {key} <- {repr(initializer)}')
    #     super().put(key, _UNINITIALIZED)
    #     self.__initializers[key] = initializer
    #     return self

    def _get(self, key):
        # print(f'_get lazy key {key}')
        v = super()._get(key)

        if v is _NOT_FOUND:
            return self._not_found()

        if v is _UNINITIALIZED:
            # print(self.__initializers)
            v = self.__initializers[key](self)
            # print(f'init key {key} -> {v}')
            self._entries[key] = v
            return v

        return v

def main():
    return

if __name__ == '__main__':
    main()
