#!/usr/bin/env python
# -*- coding: utf-8 -*-

def dict_diff(a, b):
    d = {}
    for k, v in a.items():
        if k not in b:
            d[k] = (v, None)
        if k in b and b[k] != v:
            d[k] = (v, b[k])

    for k, v in b.items():
        if k not in a:
            d[k] = (None, v)

    return d

def if_callable(f, *args, **kwds):
    if callable(f):
        return f(*args, **kwds)
    else:
        return f

def memorize(f, memory=None):
    if memory is None:
        memory = {}
    def _f(*args, **kwds):
        key = tuple(args)
        if key in memory:
            # print(f'found memorized result for key {key}, {memory[key]}')
            print(f'found memorized result for function {f.__name__} for key {key}')
            return memory[key]
        print(f'no memorized result for key {key}, call function {f.__name__}')
        result = f(*args, **kwds)
        memory[key] = result
        return result
    return _f
