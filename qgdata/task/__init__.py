#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from . import sync as _sync
from .dump import dump
from .run import clear_pending_tasks
from . import run

def sync(ctx, endpoint, table=None, kwds=None) -> None:
    # tasks = gen_tasks(ctx, endpoint)
    # run.run_tasks(ctx, tasks)

    tasks = lambda ctx: _sync.gen_tasks(ctx, endpoint, kwds)
    run.run_tasks_or_resume(ctx, tasks)


def main():
    return

if __name__ == '__main__':
    main()
