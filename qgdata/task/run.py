#!/usr/bin/env python
# -*- coding: utf-8 -*-

import traceback
from ..context import qgctx
from ..log import log
from .sync import SyncTask

def run_tasks(ctx, tasks):
    for task in tasks:
        task.run(ctx)

def run_tasks_or_resume(ctx, tasks_gen, task_table='__tasks__', min_tasks=10, num_retries=5):
    db = qgctx.db(ctx)
    t = db.get_table(task_table, create_if_not_exist=True)
    num_tasks = t.count()
    tasks = None

    def _iter_tasks():
        for obj in t.find():
            yield obj2task(obj), obj

    if num_tasks > 0:
        log.info('%d tasks found in task table, resume them...', num_tasks)
    else:
        tasks = tasks_gen(ctx)
        num_tasks = len(tasks)
        if num_tasks <= min_tasks:
            log.info('number of tasks is too small, do not use database')
            _iter_tasks = lambda: ((t, None) for t in tasks)
        else:
            _save_tasks(tasks, t)
            num_tasks = t.count()

    log.info('run %d tasks...', num_tasks)
    num_failed = num_successful = num_run = 0
    for i, (task, obj) in enumerate(_iter_tasks()):
        # log.info('running %d/%d task %s', i+1, num_tasks, obj)
        log.info('running %d/%d task %s', i+1, num_tasks, task)
        try:
            _retry(lambda: task.run(ctx), num_retries)
            if obj is not None:
                log.info('task is successful, remove it from table')
                t.delete_by_id(obj['_id'])
            num_successful += 1
            num_run += 1
        except KeyboardInterrupt as e:
            log.error('interrupted by user: %s', repr(e))
            break
        except Exception as e:
            log.error('failed to run task, error: %s', num_retries, repr(e))
            log.error(traceback.format_exc())
            num_failed += 1
            num_run += 1
            continue
        finally:
            log.info('%d tasks executed so far: %d(failed)/%d(successful), %d remaining',
                num_run, num_failed, num_successful, num_tasks - num_run)

    log.info('tasks done: %d(failed)/%d(successful)/%d(run)/%d(remaining)/%d(total)',
        num_failed, num_successful, num_run, num_tasks - num_run, num_tasks)

def _save_tasks(tasks, t, clear_existing=False):
    log.info('saving %d tasks to table %s', len(tasks), t.name())
    if clear_existing:
        t.delete_all()
    # print([ t.tojson() for t in tasks ])
    t.insert_jsons([ t.tojson() for t in tasks ])

def clear_pending_tasks(ctx, task_table='__tasks__'):
    db = qgctx.db(ctx)
    log.warn('deleting all tasks pending in table %s', task_table)
    t = db.get_table(task_table)
    t.delete_all()

def obj2task(obj):
    clz = obj['__class__']
    if clz == SyncTask.__name__:
        return SyncTask.fromjson(obj)
    else:
        raise NotImplementedError('unknown task type: %s', obj)

def _retry(f, n=5):
    for i in range(n):
        try:
            log.info('try task %d/%d', i+1, n)
            ret = f()
            log.info('task is successful')
            return ret
        except Exception as e:
            log.error('failed to run task %d/%d, error:%s', i+1, n, repr(e))
            log.error(traceback.format_exc())

    raise RuntimeError(f'failed to run task after {n} tries')

def main():
    return

if __name__ == '__main__':
    main()
