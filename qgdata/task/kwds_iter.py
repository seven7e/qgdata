class _GenEnum(object):
    def __init__(self, opts):
        self.opts = opts
        self.__curr = 0

    def get(self):
        # print('curr', self.__curr)
        return self.opts[self.__curr]

    def next(self) -> bool:
        self.__curr += 1
        if self.__curr >= len(self.opts):
            self.__curr = 0
            return True
        return False

class _GenLinearRng(object):
    def __init__(self, start, end, step):
        self.start = start
        self.end = end
        self.step = step
        self.__curr = self.start

    def get(self):
        return self.__curr

    def next(self) -> bool:
        self.__curr += self.step
        if self.__curr > self.end:
            self.__curr = self.start
            return True
        return False

def iter_kwds(kwds_range):
    if isinstance(kwds_range, dict):
        kvs = list(kwds_range.items())
    else:
        kvs = kwds_range
    names = [ k for k, _ in kvs ]
    rngs = [ v for _, v in kvs ]
    for args in iter_args(rngs):
        yield { k:v for k, v in zip(names, args) }

def iter_args(kwds_range):
    def _gen(rng):
        if isinstance(rng, list):
            return _GenEnum(rng)
        elif isinstance(rng, tuple) and len(rng) == 3:
            return _GenLinearRng(rng[0], rng[1], rng[2])
        else:
            raise RuntimeError('unknown range type: ' + str(rng))

    gen = [ _gen(rng) for rng in kwds_range ]
    while True:
        yield [ g.get() for g in gen ]

        i = len(gen)
        startover = True
        while startover and i > 0:
            i -= 1
            startover = gen[i].next()
            # print(i, gen[i].get(), startover)

        if i == 0 and startover:
            break

def _test_iter_args():
    rngs = [
        ['a', 'b', 'c'],
        # ['x', 'y'],
        (1, 5, 2),
    ]

    for i, c in enumerate(iter_args(rngs)):
        print(i, c)

def _test_iter_kwds():
    rngs = [
        ('foo', ['a', 'b', 'c']),
        # ('bar', ['x', 'y']),
        ('baz', (1, 5, 2))
    ]

    for i, c in enumerate(iter_kwds(rngs)):
        print(i, c)

def main():
    _test_iter_args()
    _test_iter_kwds()
    return

if __name__ == '__main__':
    main()
