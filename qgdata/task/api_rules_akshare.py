from datetime import datetime, timedelta
from ..log import log
from .. import conf

def _refine(db, endpoint, sig, fields):
    if endpoint == 'stock_info_sh_name_code':
        # fields.table_name = ('indicator',)
        fields.table_name = ()
        fields.rec_id = ('COMPANY_CODE', 'indicator')
        fields.param_to_save = ('indicator',)
        fields.kwds_range = [
            ('indicator', ['主板A股', '主板B股', '科创板']),
            # ('indicator', ['主板B股', '科创板']),
        ]
        fields.type = 'list'
        return fields
    elif endpoint == 'stock_info_sz_name_code':
        def _rec_id(sig, kwds):
            indicator = kwds.get('indicator', sig.parameters['indicator'].default)
            if indicator == 'A股列表':
                return ('A股代码', 'indicator')
            elif indicator == 'B股列表':
                return ('B股代码', 'indicator')
            elif indicator == 'CDR列表':
                return ('CDR代码', 'indicator')
            elif indicator == 'AB股列表':
                return ('A股代码', 'indicator')
            else:
                raise NotImplementedError('unknown indicator: ' + indicator)
        fields.rec_id = _rec_id
        fields.param_to_save = ('indicator',)
        fields.type = 'list'
        return fields
    elif endpoint == 'stock_info_a_code_name':
        fields.rec_id = ('code',)
        return fields
    elif endpoint == 'stock_zh_index_daily':
        _set_series_fields(db, sig, fields)
        fields.type = 'timeseries'
        return fields
    elif _like_multi_timeseries(sig.parameters):
        _set_ts_fields(db, sig, fields)
        fields.type = 'multi-timeseries'
        return fields
    elif _like_timeseries(sig.parameters):
        _set_ts_fields(db, sig, fields)
        fields.type = 'timeseries'
        return fields
    else:
        raise NotImplementedError('unsupported endpoint ' + endpoint)

def _has_symbol(params):
    if 'symbol' in params:
        return True
    return False

def _like_timeseries(params):
    if 'start_date' in params and 'end_date' in params:
        return True
    return False

def _like_multi_timeseries(params):
    if 'symbol' in params and _like_timeseries(params):
        return True
    return False

def _set_series_fields(db, sig, fields):
    if 'symbol' in sig.parameters:
        # fields.rec_id = ('symbol',)
        fields.series_field = ('symbol',)
        fields.param_to_save = ('symbol',)
    fields.time_field = (conf.INDEX_PH,)
    fields.rec_id += fields.series_field + fields.time_field

def _set_ts_fields(db, sig, fields):
    # print(fields)
    if 'period' in sig.parameters:
        fields.table_name = ('period',)

    if 'adjust' in sig.parameters:
        fields.kwds_range['adjust'] = ['hfq']
        fields.table_name += ('adjust',)

    if 'symbol' in sig.parameters:
        # fields.rec_id = ('symbol',)
        fields.series_field = ('symbol',)
        fields.param_to_save = ('symbol',)
        # fields.kwds_range['symbol'] = ['000001']
        fields.kwds_range['symbol'] = _read_a_stock_list(db)

    if 'end_date' in sig.parameters:
        fields.kwds_range['end_date'] = [ (datetime.now() + timedelta(hours=24)).strftime('%Y-%m-%d') ]

    fields.time_field = ('日期',)
    fields.rec_id += fields.series_field + fields.time_field

def _read_a_stock_list(db, table='akshare.stock_info_a_code_name', field='_id'):
    res = db.get_table(table).find({}, {})
    arr = [ d['_id'] for d in res ]
    # print(arr)
    log.info('read %d symbols from table %s', len(arr), table)
    return arr