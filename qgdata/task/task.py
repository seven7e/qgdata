#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import sys

class Task(object):

    def run(self, ctx):
        raise NotImplementedError()

    def tojson(self):
        obj = self._tojson()
        obj['__class__'] = self.__class__.__name__
        return obj

    def _tojson(self):
        raise NotImplementedError()

class TaskGen(object):

    def gen_tasks(self, ctx):
        raise NotImplementedError()

def main():
    return

if __name__ == '__main__':
    main()
