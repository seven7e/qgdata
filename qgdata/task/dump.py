import sys
import io
import csv

from ..log import log

def dump(db, table_name, output=sys.stdout, format='raw', limit=10, cond=None) -> None:
    # print(db.get_tables())
    q = {}
    if cond is not None:
        for name, values in cond.items():
            q[name] = {'$in': values}

    log.debug('query to dump data: %s', q)
    data = db.get_table(table_name).find(q)

    if limit is not None and limit > 0:
        data = data.limit(limit)

    def _log(i):
        if i & 0x3FFF == 0:
            log.info('%d lines written', i)

    def _write(f):
        if format == 'raw':
            for i, d in enumerate(data):
                _log(i)
                print(d, file=f)
            log.info('%d lines written in total', i)
        elif format == 'csv':
            writer = None
            for i, d in enumerate(data):
                _log(i)
                if writer is None:
                    fields = list(f for f in d if f != '_id')
                    writer = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC)
                    writer.writerow(fields)
                writer.writerow([d[f] for f in fields])
            log.info('%d lines written in total', i)
        else:
            raise RuntimeError('unknown format: ' + str(format))


    if isinstance(output, str):
        log.info('writing to file %s', output)
        with open(output, 'w') as f:
            _write(f)
    elif isinstance(output, io.TextIOWrapper):
        _write(output)
    else:
        raise RuntimeError('incorrect output: ' + repr(output))