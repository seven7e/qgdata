#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
# import inspect
# from collections import namedtuple
from collections import OrderedDict
from types import SimpleNamespace
import pandas as pd
# from datetime import datetime, timedelta

from ..context import qg as context
from ..log import log
from .. import utils
from . import api_rules_akshare, api_rules_qgapi, kwds_iter
from .task import Task, TaskGen

_SEP = '.'


class SyncTask(Task):

    def __init__(self, endpoint):
        super().__init__()
        self.endpoint = endpoint
        self.table_name = None
        self.rec_id = None
        self.time_field = None
        self.series_field = None
        self.param_to_save = None
        # self.kwds_range = None
        self.kwds = None

    def __str__(self):
        return f'{self.__class__.__name__}(endpoint={self.endpoint}' + \
            f', table_name={self.table_name}' + \
            f', rec_id={self.rec_id}, time_field={self.time_field}, series_field={self.series_field}' + \
            f', param_to_save={self.param_to_save}, kwds={self.kwds}' + \
            ')'

    def _tojson(self):
        return dict(
            endpoint = self.endpoint,
            table_name = self.table_name,
            rec_id = self.rec_id,
            time_field = self.time_field,
            series_field = self.series_field,
            param_to_save = self.param_to_save,
            kwds = self.kwds,
        )

    @classmethod
    def fromjson(cls, obj):
        t = SyncTask(obj['endpoint'])
        t.table_name = obj['table_name']
        t.rec_id = _as_tuple(obj['rec_id'])
        t.time_field = _as_tuple(obj['time_field'])
        t.series_field = _as_tuple(obj['series_field'])
        t.param_to_save = _as_tuple(obj['param_to_save'])
        t.kwds = obj['kwds']
        return t

    def run(self, ctx):
        api = context.api(ctx)
        db = context.db(ctx)

        task = self
        endpoint, table_name, rec_id, kwds = task.endpoint, task.table_name, task.rec_id, task.kwds
        # log.info('send request to %s with %', len(data), endpoint)
        data = api.call(endpoint, **kwds)
        log.info('%d records returned from %s', _num_recs(data), endpoint)
        # print(data)
        # continue
        if not isinstance(data, pd.DataFrame):
            raise NotImplementedError('only support pandas dataframe for now')

        # add param_to_save to the dataframe
        for p in task.param_to_save:
            data[p] = kwds[p]

        # print(data)

        log.info('saving data from %s to database table %s...', endpoint, table_name)
        table = db.get_table(table_name, create_if_not_exist=True)
        # print(table)
        # _validate_table_schema()
        # print(rec_id)
        table.insert_df(data, id_fields=rec_id)

class SyncTaskGen(TaskGen):

    def __init__(self, endpoint):
        super().__init__()
        self.endpoint = endpoint
        self.type = None
        self.table_name = ()
        self.rec_id = ()
        self.time_field = None
        self.series_field = None
        self.param_to_save = ()
        self.kwds_range = None
        self._latests = {}

    def __str__(self):
        return f'{self.__class__.__name__}(endpoint={self.endpoint}, type={self.type}' + \
            f', table_name={self.table_name}' + \
            f', rec_id={self.rec_id}, time_field={self.time_field}, series_field={self.series_field}' + \
            f', param_to_save={self.param_to_save}, kwds_range={self.kwds_range}' + \
            ')'

    def gen_tasks(self, ctx):
        api = context.api(ctx)
        db = context.db(ctx)

        api_lib_name = ctx.get('api_lib').strip()
        if len(api_lib_name) == 0:
            raise RuntimeError('api lib name cannot be empty')

        api_sig, param_doc = api.get_signature(self.endpoint)
        table_prefix = api_lib_name + _SEP + self.endpoint
        rec_id = self.rec_id

        tasks = []
        for kwds in kwds_iter.iter_kwds(self.kwds_range):
            task = SyncTask(self.endpoint)

            task.kwds = kwds
            task.param_to_save = utils.if_callable(self.param_to_save, kwds)
            task.time_field = utils.if_callable(self.time_field, kwds)
            task.series_field = utils.if_callable(self.series_field, kwds)
            task.table_name = _gen_table_name(table_prefix, self.table_name, kwds)
            task.rec_id = utils.if_callable(rec_id, api_sig, kwds)

            self._add_if_timeseries(db, api_sig, task)

            self._remove_invalid_kwds(api_sig, task)

            tasks.append(task)

        log.info('%d tasks generated for endpoint %s', len(tasks), self.endpoint)
        return tasks

    def _add_if_timeseries(self, db, api_sig, task):
        table_name = task.table_name
        kwds = task.kwds
        time_field, series_field = task.time_field, task.series_field
        if time_field is not None:
            table = db.get_table(table_name, create_if_not_exist=True)
            if series_field is not None:
                series_filter = { k: kwds[k] for k in series_field }
                log.debug('read table %s for last updated time of series %s', table_name, series_filter)
                latest = self._get_latest(db, table_name, time_field, series_field, series_filter)
                if latest is None:
                    log.debug('failed to read last updated time from cache, get from table...')
                    latest = table.find_max(time_field, time_field, series=series_filter)
                    # print(latest)
                else:
                    log.debug('successfully got updated time from cache')
            else:
                series_filter = None
                log.debug('read table %s for last updated time', table_name)
                latest = table.find_max(time_field, time_field, series=series_filter)

            if latest is None:
                log.debug('table %s is empty', table_name)
                kwds['start_date'] = '1700-01-01'
            else:
                log.debug('table %s for series %s is updated up to %s', table_name, series_filter, latest)
                kwds['start_date'] = ''.join(latest)
            # raise NotImplementedError()

    def _get_latest(self, db, table_name, time_fields, series_fields, series):
        k = (table_name, series_fields, tuple(series[f] for f in series_fields))
        # print('get latest k', k)
        if k not in self._latests:
            log.info('initialize cache for latest updated time for all series')
            table = db.get_table(table_name, create_if_not_exist=True)
            maxs = table.groupby(series_fields, { f: 'max' for f in time_fields })
            # print(maxs)
            # maxs like `{('000002',): {'日期': '2021-09-24'}, ...}`
            for s, agg in maxs.items():
                self._latests[(table_name, series_fields, s)] = tuple(agg[f] for f in time_fields)
            log.info('initialization successful, got %d records', len(self._latests))
            # print(self._latests)
            # print(k in self._latests)
        # sys.exit()

        return self._latests.get(k, None)

    def _remove_invalid_kwds(self, sig, task):
        param = sig.parameters.keys()
        keys = list(task.kwds.keys())
        for k in keys:
            if k not in param:
                log.warning('remove kwd %s as it is not in param list %s', k, param)
                del task.kwds[k]

def _as_tuple(x):
    if isinstance(x, list):
        return tuple(x)
    else:
        return x

def _new_task_fields(endpoint):
    # return _task_fields(None, None, None, None)
    return SyncTaskGen(endpoint)

def gen_tasks(ctx, endpoint, kwds_overwrites):
    task_gen = _create_task_gen(ctx, endpoint, kwds_overwrites)
    # log.info('sync data type: %s', type_)
    log.info('sync task generator: %s', task_gen)
    return task_gen.gen_tasks(ctx)

def _create_task_gen(ctx, endpoint, kwds_overwrites):
    api = context.api(ctx)
    db = context.db(ctx)
    api_lib = ctx.get('api_lib')

    api_sig, param_doc = api.get_signature(endpoint)
    # sys.exit()
    gen = _new_task_fields(endpoint)
    gen.type = 'list'
    # fields.kwds_range = [ (k, v) for k, v in param_doc.items() ]

    log.info('keyword argument overwrites: %s', kwds_overwrites)
    gen.kwds_range = OrderedDict()
    for name, value in api_sig.parameters.items():
        # print(value)
        # print(value.default)
        # print(value.default is not value.empty)
        if name in param_doc:
            gen.kwds_range[name] = param_doc[name]
        elif value.default is not value.empty:
            gen.kwds_range[name] = [ value.default ]

    if api_lib == 'akshare':
        gen = api_rules_akshare._refine(db, endpoint, api_sig, gen)
    elif api_lib == 'qgapi':
        gen = api_rules_qgapi._refine(db, endpoint, api_sig, gen)

    # kwds overwrites have highest priority
    for name, value in kwds_overwrites.items():
        gen.kwds_range[name] = value.split(',')

    return gen

# def _run_tasks(ctx, tasks):
    # _sync_list(ctx, endpoint, fields)
    # if type_ == 'list':
    #     _sync_list(ctx, endpoint, fields)
    # elif type_ == 'timeseries':
    #     _sync_timeseries(ctx, endpoint, table)
    # elif type_ == 'multi-timeseries':
    #     # _sync_multi_timeseries(ctx, endpoint, table, fields)
    #     _sync_list(ctx, endpoint, fields)
    # elif type_ == 'snapshot':
    #     _sync_snapshot(ctx, endpoint, table)
    # elif type_ == 'snapshot-history':
    #     _sync_snapshot_history(ctx, endpoint, table)
    # elif type_ == 'events':
    #     _sync_events(ctx, endpoint, table)
    # else:
    #     raise RuntimeError('cannot guess sync type from endpoint ' + endpoint)


# def _sync_list(ctx, endpoint, fields):

def _num_recs(data):
    if isinstance(data, pd.DataFrame):
        return len(data.index)
    return len(data)

def _gen_table_name(prefix, fields, kwds):
    if len(fields) == 0:
        return prefix

    values = [ prefix ] \
        + [ str(kwds[f]).strip() if str(kwds[f]).strip() != '' else '_' for f in fields ]

    # print(values)
    return '.'.join(values)

# def _sync_multi_timeseries(ctx, endpoint, table, fields):
#     db = context.db(ctx)
#     # group_field = fields.group
#     # time_field = fields.time
#     # for group in _iter_groups(ctx, group):
#     #     _sync_timeseries(ctx, endpoint, )

def main():
    return

if __name__ == '__main__':
    main()
