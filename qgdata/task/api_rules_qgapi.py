from datetime import datetime, timedelta
from ..log import log

def _refine(db, endpoint, sig, fields):
    if endpoint == 'get_zh_a_index_list':
        def _rec_id(sig, kwds):
            return ('指数代码', )
        fields.rec_id = _rec_id
        # fields.param_to_save = ('indicator',)
        fields.type = 'list'
        return fields
    # elif _like_multi_timeseries(sig.parameters):
    #     _set_ts_fields(db, sig, fields)
    #     fields.type = 'multi-timeseries'
    #     return fields
    # elif _like_timeseries(sig.parameters):
    #     _set_ts_fields(db, sig, fields)
    #     fields.type = 'timeseries'
    #     return fields
    else:
        raise NotImplementedError('unsupported endpoint ' + endpoint)

def _like_timeseries(params):
    if 'start_date' in params and 'end_date' in params:
        return True
    return False

def _like_multi_timeseries(params):
    if 'symbol' in params and _like_timeseries(params):
        return True
    return False

def _set_ts_fields(db, sig, fields):
    # print(fields)
    if 'period' in sig.parameters:
        fields.table_name = ('period',)

    if 'adjust' in sig.parameters:
        fields.kwds_range['adjust'] = ['hfq']
        fields.table_name += ('adjust',)

    if 'symbol' in sig.parameters:
        # fields.rec_id = ('symbol',)
        fields.series_field = ('symbol',)
        fields.param_to_save = ('symbol',)
        # fields.kwds_range['symbol'] = ['000001']
        fields.kwds_range['symbol'] = _read_a_stock_list(db)

    if 'end_date' in sig.parameters:
        fields.kwds_range['end_date'] = [ (datetime.now() + timedelta(hours=24)).strftime('%Y-%m-%d') ]

    fields.time_field = ('日期',)
    fields.rec_id += fields.series_field + fields.time_field

def _read_a_stock_list(db, table='akshare.stock_info_a_code_name', field='_id'):
    res = db.get_table(table).find({}, {})
    arr = [ d['_id'] for d in res ]
    # print(arr)
    log.info('read %d symbols from table %s', len(arr), table)
    return arr