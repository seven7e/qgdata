#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
from collections import OrderedDict
from typing import List
import argparse

from . import conf, api, db, task
from .db import mongodb
from .api.akshare import AkshareAPI
from .api.qgapi import QGApi
from .log import log
from .context import qg as context

def run(argv: List[str]) -> None:
    load_env()
    args, unknown = parse_args(argv)
    log.debug('command line argument parsed: %s', args)
    log.debug('unknown args: %s', unknown)
    ctx = _create_ctx(args)
    args.handle(ctx, args, unknown)

def load_env(dir='.') -> None:
    conf.load(dir)
    # print(conf)

def parse_args(argv):
    parser = argparse.ArgumentParser(prog="qgd",
        description="Manage market data from various APIs with database")

    # parser.add_argument('--foo', action='store_true', help='foo help')
    # parser.add_argument('--instrument', help='instrument symbol')
    # parser.add_argument('--end-date', help='end date')
    # parser.add_argument('--start-date', help='start date')
    parser.add_argument('--api-lib', default='akshare', help='API libraray to use')
    parser.add_argument('--db-engine', default='mongodb', help='which database to use')

    subparsers = parser.add_subparsers(
            dest='cmd',
            title='subcommands',
            description='subcommands',
            help='additional help')

    parser_desc = subparsers.add_parser('desc', help='list all endpoints or describe an endpoint')
    parser_desc.add_argument('endpoint', nargs='?', default=None, help='name of the endpoint to describe')
    parser_desc.add_argument('-s', '--search', default=None, help='keyword to search in endpoints')
    parser_desc.set_defaults(handle=_desc)

    parser_sample = subparsers.add_parser('sample', help='read a small set of data from an API as sample')
    parser_sample.add_argument('endpoint', help='the endpoint to call')
    parser_sample.set_defaults(handle=_sample)

    # create the parser for the "b" command
    parser_dump = subparsers.add_parser('dump', help='dump a dataset from database')
    parser_dump.add_argument('table', help='table to dump')
    parser_dump.add_argument('-n', '--num', default=10, type=int, help='maximum number of records. 0 means no limit.')
    parser_dump.add_argument('-o', '--output', default='-', help='output to file')
    parser_dump.add_argument('-d', '--output-dir', help='output to dir with the table name as filename')
    parser_dump.add_argument('-f', '--format', default='raw', help='output format')
    parser_dump.add_argument('-c', '--condition', nargs='*', help='condition to query')
    parser_dump.set_defaults(handle=_dump)

    parser_sync = subparsers.add_parser('sync', help='synchronize data from API to db')
    parser_sync.add_argument('endpoint', help='the endpoint to call')
    parser_sync.set_defaults(handle=_sync)

    parser_sync = subparsers.add_parser('clear-pending-tasks', help='clear database table of tasks')
    parser_sync.set_defaults(handle=_clear_pending_tasks)

    return parser.parse_known_args(argv)

def _create_ctx(args):
    if args.api_lib == 'akshare':
        def _create_api(ctx):
            return AkshareAPI(min_delay=conf.must_int('API_DELAY'))
    elif args.api_lib == 'qgapi':
        def _create_api(ctx):
            return QGApi(min_delay=conf.must_int('API_DELAY'))
    else:
        raise RuntimeError('unknown api lib ' + args.api_lib)

    if args.db_engine == 'mongodb':
        def _create_db(ctx):
            return mongodb.create()
    else:
        raise RuntimeError('unknown db engine' + args.db_engine)

    return context.lazy_ctx_with(
            context.ctx_with(None, api_lib=args.api_lib, db_engine=args.db_engine),
            api=_create_api,
            db=_create_db)

def _sample(ctx, args, unknown):
    # print(args, unknown)
    # print(conf.mongodb_auth())
    # _db = mongodb.create()
    api = context.api(ctx)
    # db = context.db(ctx)
    # _api = ak.get_roll_yield()
    # _api = ak.call('stock_zh_a_hist')
    kwds = {}
    def _add_if(k, v):
        if v is not None:
            kwds[k] = v
    # _add_if('start_date', args.start_date)
    # _add_if('end_date', args.end_date)
    # _add_if('symbol', args.instrument)

    unknown_args, unknown_kwds = _opts2args(unknown)
    kwds.update(unknown_kwds)
    _api = api.sample(args.endpoint, *unknown_args, **kwds)
    # api =

def _desc(ctx, args, unknown):
    api = context.api(ctx)
    endpoint = args.endpoint
    if endpoint is None:
        api.list_apis(args.search)
    else:
        api.desc_api(endpoint)

def _sync(ctx, args, unknown):
    unknown_args, unknown_kwds = _opts2args(unknown)
    task.sync(ctx, args.endpoint, kwds=unknown_kwds)

def _clear_pending_tasks(ctx, args, unknown):
    task.clear_pending_tasks(ctx)

def _dump(ctx, args, unknown):
    db = context.db(ctx)

    limit = None
    if args.num > 0:
        limit = args.num

    output = args.output
    if args.output_dir is not None:
        output = os.path.join(args.output_dir, args.table+'.'+args.format)
    elif args.output == '-':
        output = sys.stdout

    cond = OrderedDict()
    if args.condition is not None:
        for c in args.condition:
            name, values = c.split('=', 1)
            cond[name] = values.split(',')

    task.dump(db, args.table, output=output, format=args.format, limit=limit, cond=cond)

def _opts2args(opts):
    key = None
    args = []
    kwds = {}
    for s in opts:
        if s.startswith('--'):
            if key is not None:
                kwds[key] = True
                key = None
            key = s[2:].replace('-', '_')
        else:
            if key is None:
                args.append(s)
            else:
                kwds[key] = s
                key = None
    # print(args, kwds)
    return args, kwds

def main(argv: List[str]) -> None:
    try:
        run(argv)
    except Exception as e:
        log.critical('an error occurred: %s', repr(e))
        raise e

if __name__ == '__main__':
    main()
