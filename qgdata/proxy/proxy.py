#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from queue import Queue
import requests
import time

from ..log import log
from fake_useragent import UserAgent
from requests.exceptions import ConnectTimeout, ProxyError

proxy_candidates = Queue()
active_proxies = Queue()

_MIN_PROXIES = 4

def get_proxy(f, proxy_generator, max_err_cnt=5):
    def _get_proxy():
        if (nq := active_proxies.qsize()) < _MIN_PROXIES:
            log.info('%d active proxies, less than %d, will get a new one', nq, _MIN_PROXIES)
            while True:
                ip = next(proxy_generator)
                # if True:
                if check_ip(ip):
                    log.debug('found active proxy: %s', ip)
                    time.sleep(1)
                    break
                log.debug('proxy not available, fetch next...')
            proxy = { 'http': 'http://' + ip, 'https': 'https://' + ip }
            err_cnt = 0
        else:
            proxy, err_cnt = active_proxies.get()
            log.debug('get proxy from active queue: %s', proxy)
        return proxy , err_cnt

    def _proxy_request(*args, **kwds):
        proxy, err_cnt = _get_proxy()
        log.debug('get proxy: %s', proxy)
        try:
            kwds['timeout'] = 10 * (err_cnt + 1)
            ret = f(*args, proxies=proxy, **kwds)
            log.debug('proxied request status: %d', ret.status_code)
            if ret.status_code > 399:
                raise RuntimeError('failed status: ' + str(ret.status_code))
            active_proxies.put((proxy, 0))
            log.debug('put proxy %s to active queue (%d total)', proxy, active_proxies.qsize())
            return ret
        except Exception as e:
            err_cnt += 1
            log.error('proxy has failed for %d consecutive times. error: %s', err_cnt, repr(e))
            if err_cnt > max_err_cnt:
                log.error('proxy exceeds maximum error count %d, put to candidate queue (%d total)',
                    max_err_cnt, proxy_candidates.qsize())
                proxy_candidates.put((proxy, err_cnt))
            else:
                log.debug('proxy error count no larger than maximum %d, put back to active queue (%d total)',
                    max_err_cnt, active_proxies.qsize())
                active_proxies.put((proxy, err_cnt))
            raise

    return _proxy_request

_CHECK_URL = 'http://icanhazip.com'

def is_check_ip_url(url):
    return _CHECK_URL in url

def check_ip(ippost_info, headers=None, timeout=20):
    if headers is None:
        headers = { 'User-Agent': UserAgent().random }
    # http://icanhazip.com IP测试网址
    proxies = {'http': 'http://' + ippost_info, 'https': 'https://' + ippost_info}
    log.info('checking ip %s', ippost_info)
    try:
        time.sleep(0.1)
        # 发送测试请求
        r = requests.get(_CHECK_URL, headers=headers, proxies=proxies, timeout=timeout)
        if r.status_code == 200:
            log.info('有效IP：' + ippost_info)
            # with open('xila_https_list.txt', 'a') as f:
            #     f.write(ippost_info)
            #     f.write('\n')
            return True
        else:
            log.debug('无效IP：' + ippost_info)
            return False
    except KeyboardInterrupt as e:
        log.warning('interrupted by user %s', repr(e))
        raise e
    except (ConnectTimeout, ProxyError) as e:
        log.debug('无效IP：' + ippost_info + ', error: ' + repr(e))
        return False

def main():
    return

if __name__ == '__main__':
    main()
