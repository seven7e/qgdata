#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
爬取国内高匿代理IP
author: Python当打之年
'''
import random
import time
import more_itertools as it
import requests
from bs4 import BeautifulSoup
from fake_useragent import UserAgent

from ..log import log
from .proxy import get_proxy, check_ip

_URL = 'http://www.xiladaili.com/gaoni/'

def is_xila_url(url):
    return _URL in url

def use_xila_proxy(f):
    headers = { 'User-Agent': UserAgent().random }
    return get_proxy(f, get_ipinfo(_URL, headers, max_page_no=50))

def get_ipinfo(url, headers, total_ips=None, max_page_no=500):
    ipinfo = []
    print(headers)
    # 爬取前50页代理
    pages = list(range(1, max_page_no+1))
    random.shuffle(pages)
    max_valid_page = max_page_no
    for i in pages:
        if i > max_valid_page:
            log.debug('page number %d is larger than the maximum %d, skip', i, max_valid_page)
            continue
        html = None
        try:
            if i == 1:
                url0 = url
            else:
                url0 = url + str(i) + '/'
            time.sleep(random.uniform(1.5, 4.2))
            log.info('reading ip list from %s', url0)

            html = requests.get(url0, headers=headers, timeout=10)
        except KeyboardInterrupt as e:
            log.warning('interrupted by user %s', repr(e))
            break
        except:
            log.error('failed to read ip list for %s', url0)
            continue

        soup = BeautifulSoup(html.text, 'lxml')
        tr_list = soup.select_one('.fl-table').select_one('tbody').select('tr')
        num_ips = num_good = 0
        for td_list in tr_list:
            num_ips += 1
            # 过滤，仅获取高匿、有效期超过1天的IP
            tds = td_list.select('td')
            if '高匿' in tds[2].text: #and '天' in tds[5].text:
                    ipport = tds[0].text
                    # ipinfo.append(ipport)
                    num_good += 1
                    yield ipport
        log.debug('page %d: got %d useful ips among %d', i, num_good, num_ips)

        if total_ips is not None and num_good >= total_ips:
            log.info('got enough ips: %d, will return', num_good)
            break

        if num_ips <= 0:
            max_valid_page = i - 1
            log.debug('no ips in this page, set max valid page number to %d', max_valid_page)

    # 返回满足要求IP
    # return ipinfo

def _test_proxy():
    headers = { 'User-Agent': UserAgent().random }
    ips = get_ipinfo(_URL, headers)
    # print(ips)
    for ip in it.take(90, ips):
        headers = { 'User-Agent': UserAgent().random }
        check_ip(ip, headers)

def main():
    _test_proxy()
    return

if __name__ == '__main__':
    main()
