#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import time
import requests
import re
from collections import OrderedDict
from datetime import datetime, timedelta
from dateutil import parser
import inspect
import pandas as pd

from ..context import lib as context
from .. import utils
from ..proxy import proxy, xila
from ..log import log

class _Unspecified(): pass
_UNSPECIFIED = _Unspecified()

_old_get = requests.get

def _patch(f, timeout, **kwds_overwrites):
    kwds_overwrites['timeout'] = timeout

    if os.environ.get('DISABLE_PROXY', 'false').lower() != 'true':
        log.info('proxy disabled, will send requests directly')
        proxied = xila.use_xila_proxy(f)
    else:
        proxied = f

    def _patched_f(url, *args, **kwds):
        for k, v in kwds_overwrites.items():
            if k not in kwds:
                kwds[k] = v
        log.debug(f'monkey patched {f.__name__}, url: {url}, {args}, {kwds}')
        if xila.is_xila_url(url) or 'proxies' in kwds:
            log.debug('skip proxy as it is request to get proxies or proxy already set')
            res = f(url, *args, **kwds)
            log.debug('original request done, status: ' + str(res.status_code))
            return res
        else:
            try:
                res = proxied(url, *args, **kwds)
                log.debug('proxied request returned, status %d', res.status_code)
                kwds_overwrites['timeout'] = max(kwds_overwrites['timeout'] / 2, timeout)
                return res
            # except requests.ConnectTimeout as e:
            except Exception as e:
                kwds_overwrites['timeout'] = min(kwds_overwrites['timeout'] * 2.0, 120)
                log.error('http request failed: %s, current timeout: %f', repr(e), kwds_overwrites['timeout'])
                raise e

    return _patched_f

requests.get = _patch(requests.get, timeout=10.)

class _API(object):

    def __init__(self, module, min_delay, request_time_included=True):
        self._module = module
        self._min_delay = min_delay * 1_000_000
        self._request_time_included = request_time_included
        self.__last_called = _now() - self._min_delay

    def _wait(self):
        now = _now()
        # print(self._min_delay)
        # print(self._request_time_included)
        if self._request_time_included:
            delay = (self._min_delay - (now - self.__last_called)) / 1_000_000_000.0
        else:
            delay = self._min_delay / 1_000_000_000.0
        # print(delay)

        if delay > 0:
            log.debug('wait for %f seconds for traffic control', delay)
            time.sleep(delay)

        self.__last_called = now

    def call(self, api, *args, **kwds):
        self._wait()
        log.debug('invoke `%s` with params %s, %s', api, str(args), str(kwds))
        func = getattr(self._module, api)
        if not callable(func):
            raise RuntimeError(f'{api}` is not a function')
        # print(func)
        # return pd.DataFrame()
        return func(*args, **kwds)

    def get_signature(self, endpoint):
        func = getattr(self._module, endpoint)
        sig = inspect.signature(func)
        param_doc = self._parse_doc_for_param(func)
        return sig, param_doc

    def _parse_doc_for_param(self, func):
        return {}

    def list_apis(self, match=None):
        print(f'{self._module.__name__} {self._module.__version__}')
        # print("Module description:")
        print(self._module.__doc__)
        print()
        for name, obj in inspect.getmembers(self._module):
            if name.startswith('__') and name.endswith('__'):
                continue
            if match is not None and (obj.__doc__ is None or match not in obj.__doc__):
                continue
            summary = obj.__doc__.strip().split('\n', 1)[0] if obj.__doc__ is not None else ''
            print(f'{name}\t{summary}')

    def desc_api(self, api):
        func = getattr(self._module, api)
        type_ = 'module' if inspect.ismodule(func) \
            else 'function' if inspect.isfunction(func) \
            else 'unknown'
        print(f'{api}: {type_}')
        print('Description:')
        print(func.__doc__.strip('\n'))
        if type_ == 'function':
            spec = inspect.signature(func)
            print()
            print('Signature:')
            print('    ' + str(spec))
            code = inspect.getsource(func)
            # print(code)
            right = code.rfind(']')
            right = code[:right].rfind(']')
            left = code.rfind('[')
            if right > 0 and left > 0:
                columns = code[(left+1) : right]
                print()
                print('Returns:')
                for c in columns.strip().split(','):
                    print('    ' + c.strip().strip('"'))

    def sample(self, api: str, *args, **kwds) -> None:
        func = getattr(self._module, api)
        # print(func.__doc__)
        # print(self._module.stock_em_lrb.__doc__)
        if not callable(func):
            raise RuntimeError(f'{api}` is not a function')
        # param_def = inspect.getfullargspec(func)
        sig = inspect.signature(func)
        log.debug('function `%s` signature: %s', api, str(sig))
        log.debug('invoke `%s` with argument overwrites: %s, %s', api, str(args), str(kwds))
        kwds = self._gen_args(api, sig, args, kwds)
        log.debug('invoke `%s` with real argument: %s', api, str(kwds))
        ret = func(*args, **kwds)
        if isinstance(ret, pd.DataFrame):
            # print(ret.columns)
            print('summary:')
            try:
                print(ret.describe().transpose())
            except Exception as e:
                print('failed to summarize dataframe, error:')
                print(repr(e))
        print('')
        print('dataframe:')
        print(ret)

    def _gen_args(self, api, sig, args, kwds):
        params = sig.parameters
        # print(params)
        # print(kwds)
        kwds = merge_args_into_kwds(params, args, kwds)
        # print(kwds)
        # sys.exit(1)
        kwds_gen = OrderedDict()
        ctx = self._args_ctx(api, params, kwds)
        # ctx.get('end_date')
        # return
        for position, (param_name, param_spec) in enumerate(params.items()):
            # print(param_name)
            # print(repr(param_spec))
            # print(param_spec.name, param_spec.default, param_spec.annotation)
            # param_value = self._gen_arg(api, param_name, position, kwds, kwds_gen)
            param_value = ctx.get(param_name)
            if param_value is _UNSPECIFIED:
                if param_spec.default is inspect.Parameter.empty:
                    raise RuntimeError(f'no specified or default value for {param_name}')
            else:
                kwds_gen[param_name] = _format(param_name, param_value)
        return kwds_gen

    def _args_ctx(self, api, params, kwds_overwrites):
        c = context.MapContext(
            api=api,
            params=params,
            kwds_overwrites=kwds_overwrites)
        inits = {}
        for position, (param_name, param_spec) in enumerate(params.items()):
            init = _check_specified(param_name, kwds_overwrites, self._gen_default_arg(api, param_name))
            inits[param_name] = init
        c = context.LazyContext(c, **inits)
        return c

    def _gen_default_arg(self, api, param_name):
        return lambda _: _UNSPECIFIED

def _check_specified(param_name, kwds_overwrites, gen_default):
    def _check(ctx):
        # if param_name in kwds_already_generated:
        #     raise RuntimeError(f'duplicated param name: {param_name}')
        # kwds = ctx.must_get('kwds_overwrites')
        # print(kwds_overwrites)
        if param_name in kwds_overwrites:
            return kwds_overwrites[param_name]

        return gen_default(ctx)
    return _check

def _now():
    return time.time_ns()

def _format(param_name, value):
    return value

def merge_args_into_kwds(params, args, kwds):
    kwds_merged = OrderedDict()
    for position, param_name in enumerate(params.keys()):
        if position < len(args):
            if param_name in kwds_merged:
                raise RuntimeError(f'multiple value for param@{position}:{param_name}')
            kwds_merged[param_name] = args[position]
        elif param_name in kwds:
            kwds_merged[param_name] = kwds[param_name]
    return kwds_merged

