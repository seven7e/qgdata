#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import sys
import re
from collections import OrderedDict
from datetime import datetime, timedelta
from dateutil import parser
import inspect
import pandas as pd
import akshare as ak

from .api import _API, _UNSPECIFIED
from ..context import lib as context
from ..log import log
from .. import utils

# monkey patch `_code_id_map` to avoid repeative request for code map when reading history data for each symbol
import akshare.stock_feature.stock_em_hist
akshare.stock_feature.stock_em_hist._code_id_map = utils.memorize(akshare.stock_feature.stock_em_hist._code_id_map)

_TODAY = datetime.today()

_PARAM_DOC = re.compile(r':param\s+([\w]+):\s*choice\s+of\s+{([^}]+)}')

class AkshareAPI(_API):

    def __init__(self, min_delay=100, request_time_included=False):
        super().__init__(ak, min_delay=min_delay, request_time_included=request_time_included)

    def get_roll_yield(self):
        # get_roll_yield_bar_df = ak.get_roll_yield_bar(type_method="date", var="RB", start_day="20180618", end_day="20180718", plot=True)
        # print(get_roll_yield_bar_df)
        print('get...')
        stock_zh_a_hist_df = ak.stock_zh_a_hist(symbol="000001", period="daily", start_date="20170301", end_date='20210907', adjust="")
        print(stock_zh_a_hist_df)

    def _parse_doc_for_param(self, func):
        doc = func.__doc__
        # print(doc)
        param_doc = {}
        for line in doc.split('\n'):
            line = line.strip()
            # print(line)
            match = _PARAM_DOC.match(line)
            # print(match)
            if match is not None:
                # print(match.group(0))
                # print(match.group(1))
                # print(match.group(2))
                # print(match.groups(1))
                # print(match.groups(2))
                # print(match.groups)
                param_name = match.group(1)
                choices = match.group(2).strip()
                choices = re.sub(r':\s*[^\s]+(?=,|$)', '', choices)
                choices = eval('[' + choices + ']')
                param_doc[param_name] = choices
        # print(param_doc)
        return param_doc

    def _gen_default_arg(self, api, param_name):
        # if param_name in ('symbol') and api != 'stock_zh_index_daily':
            # return _gen_default_symbol
        if param_name in ('period'):
            return _gen_default_period
        elif param_name in ('start_date'):
            return _gen_default_start_date
        elif param_name in ('end_date'):
            return _gen_default_end_date
        elif param_name in ('adjust'):
            return _gen_default_adjust

        return lambda _: _UNSPECIFIED

def _gen_default_symbol(ctx):
    return '000002'
def _gen_default_period(ctx):
    return 'daily'
def _gen_default_start_date(ctx):
    end_date = ctx.get('end_date')
    start_date = parser.parse(end_date) - timedelta(days=10)
    return start_date.strftime('%Y%m%d')
def _gen_default_end_date(ctx):
    return _TODAY.strftime('%Y%m%d')
def _gen_default_adjust(ctx):
    return 'qfq'


def main():
    return

if __name__ == '__main__':
    main()
