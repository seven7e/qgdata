#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import requests
from fake_useragent import UserAgent
from bs4 import BeautifulSoup
import pandas as pd

from .api import _API

__version__ = '0.0.1'

# __all__ = ['get_zh_a_index_list']

def get_zh_a_index_list():
    ''' Get index list of china A stock
    '''

    url = 'https://www.joinquant.com/data/dict/indexData'
    headers = { 'User-Agent': UserAgent().random }
    r = requests.get(url, headers=headers)
    # print(r.encoding)
    r.encoding = 'utf-8'
    soup = BeautifulSoup(r.text, 'lxml')
    # tr_list = soup.select_one('.fl-table').select_one('tbody').select('tr')
    # print(soup)
    table = soup.select_one('table')
    # print(table)
    header = [ t.getText() for t in table.select_one('thead').select_one('tr').select('th') ]
    # print(header)
    rows = [ [ t.getText() for t in row.select('td') ] \
        for row in table.select_one('tbody').select('tr') ]
    # print(rows[:10])
    df = pd.DataFrame(rows, columns=header)
    # print(df)
    return df

class QGApi(_API):

    def __init__(self, min_delay=100, request_time_included=False):
        super().__init__(sys.modules[__name__], min_delay=min_delay, request_time_included=request_time_included)

def main():
    get_zh_a_index_list()
    return

if __name__ == '__main__':
    main()
